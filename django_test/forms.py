# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class MyRegistrationFrom(UserCreationForm):
    email = forms.EmailField(required=True)
    fields = ('username', 'email', 'password1', 'password2')

    def save(self, commit=True):
        user = UserCreationForm.save(self, commit=False)
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user


class ContactForm1(forms.Form):
    subject = forms.CharField(max_length=100)


class ContactForm2(forms.Form):
    sender = forms.EmailField()


class ContactForm3(forms.Form):
    message = forms.CharField(widget=forms.Textarea)


