#!/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
"""
Created on Thu Jul 31 18:55:50 2014

@author: rad.cirskis
"""

import os
import dj_database_url
from settings import PROJECT_DIRECTORY

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Rad Cirskis', 'nad2000@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'sqlite:////' + os.path.join(PROJECT_DIRECTORY, 'storage.db'),  # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}


UPLOAD_FILE_PTTERN = "uploaded_files/%s_%s"
