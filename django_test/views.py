# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
#from django.contrib.auth.forms import UserCreationForm
from forms import  MyRegistrationFrom

from django.http import HttpResponse
from django.template import RequestContext
from django.contrib import messages

# for using WizardView
from django.contrib.formtools.wizard.views import SessionWizardView # or CookieWizardView
from django.core.mail import send_mail

import logging
logr = logging.getLogger(__name__)

def login(req):
    c = {}
    c.update(csrf(req))
    return render_to_response('login.html', c)


def auth_view(req):
    username = req.POST.get('username', '')
    password = req.POST.get('password', '')
    try:
        user = auth.authenticate(username=username, password=password)
    except:
        user = None

    if user is not None:
        auth.login(req, user)
        return HttpResponseRedirect('/accounts/loggedin')
    else:
        return HttpResponseRedirect('/accounts/invalid')


def loggedin(req):
    return render_to_response('loggedin.html',
                              {'full_name': req.user.username})


def invalid(req):
    auth.logout(req)
    return render_to_response('invalid_login.html')


def logout(req):
    auth.logout(req)
    return render_to_response('logout.html')


def register_user(req):
    if req.method == 'POST':
        form = MyRegistrationFrom(req.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/accounts/register_success')

    args = {}
    args.update(csrf(req))

    args['form'] = MyRegistrationFrom()

    return render_to_response("register.html", args)


def register_success(req):
    return render_to_response("register_success.html")


# for Wizzard view need to create class based view:
class ContactWizard(SessionWizardView):
    template_name = "contact_form.html"

    def done(self, form_list, **kwargs):
        form_data = process_form_data(form_list)

        return render_to_response("done.html", {"form_data": form_data})


def process_form_data(form_list):
    form_data = [form.cleaned_data for form in form_list]

    subject, sender, message = \
        form_data[0]["subject"], form_data[1]["sender"], form_data[2]["message"]

    logr.debug("subject: {}", subject)
    logr.debug("sender: {}", sender)
    logr.debug("message: {}", message)

    send_mail(subject, message, sender, ["contact-form-forever@mailinator.com"], fail_silently=False)

    return form_data
