#!/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
"""
Created on Thu Jul 17 16:04:31 2014

@author: rad.cirskis
"""

from django import forms
from models import UserProfile

class UserProfileForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        fields = ('likes_cheese', 'favorite_hamster_name')
