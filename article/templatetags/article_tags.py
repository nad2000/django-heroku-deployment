# -*- coding: utf-8 -*-
from django import template

register = template.Library()

@register.filter(name='article_shorten_body')
def article_shorten_body(bodytext, length): # can be any name
    if len(bodytext) > int(length):
        return "%s ..." % bodytext[:int(length)]
    else:
        return bodytext
