from django.db import models
from time import time


def get_upload_file_name(instance, filename):
    return "uploaded_files/%s_%s" % (str(time()).replace('.', '_'), filename)


# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    pub_date = models.DateTimeField('data published')
    likes = models.IntegerField(default=0)
    thumbnail = models.FileField(upload_to=get_upload_file_name)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/articles/%i/" % self.id


class Comment(models.Model):
    name = models.CharField(max_length=200)
    #first_name = models.CharField(max_length=200)
    #second_name = models.CharField(max_length=200)
    body = models.TextField()
    pub_date = models.DateTimeField('date published')
    article = models.ForeignKey(Article)

    def __unicode__(self):
        return self.title


class Rating(models.Model):
    stars = models.IntegerField(default=0)
    article = models.ForeignKey(Article)
