from django.shortcuts import render_to_response, render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.utils import timezone

from django.template import RequestContext
from django.contrib import messages

from forms import ArticleForm, CommentForm
from article.models import Article, Comment
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf

import logging
logr = logging.getLogger(__name__)

# Create your views here.


def articles(request):
    language = 'en-gb'
    session_language = 'en-gb'

    if 'lang' in request.COOKIES:
        language = request.COOKIES['lang']

    if 'lang' in request.session:
        session_language = request.session['lang']


    args = {}
    args.update(csrf(request))
    args.update({
        'articles': Article.objects.all(),
        'language': language,
        'session_language': session_language})

    return render_to_response('articles.html', args)


def article(request, article_id=1):
    #return render_to_response('article.html',
                              #{'article': Article.objects.get(id=article_id) },
                              #context_instance=RequestContext(request))
    return render(request, 'article.html',
                  {'article': Article.objects.get(id=article_id) })


def language(req, language='en-gb'):
    response = HttpResponse("setting language to %s"  % language)
    response.set_cookie('lang', language)

    return response


### Procedural approach:

def hello(request):
    name = "Mike"
    html = "<html><body>Hi %s, thsi seams to have worked!</body></html>" % name
    return HttpResponse(html)


from django.template.loader import get_template
from django.template import Context

def hello_template(request):
    name = "Mike"
    t = get_template('hello.html')
    html = t.render(Context({'name': name}))
    return HttpResponse(html)


## OOP approach:

from django.views.generic.base import TemplateView

class HelloTemplate(TemplateView):

    template_name = "hello_class.html"

    def get_context_data(self, **kwargs):
        context = TemplateView.get_context_data(self, **kwargs)
        context['name'] = "Mike"
        return context

## With shortcuts:

def hello_template_simple(request):
    name = "Mike"
    return render_to_response('hello.html', {'name': name})


def create(req):
    if req.POST:
        form = ArticleForm(req.POST, req.FILES)
        if form.is_valid():
            form.save()

            return HttpResponseRedirect('/articles/all/')
    else:
        form = ArticleForm()

    args = {}
    args.update(csrf(req))

    args['form'] = form

    return render_to_response('create_article.html', args)


def like_article(req, article_id):

    if article_id:
        a = Article.objects.get(id=article_id)
        a.likes += 1
        a.save()

    return HttpResponseRedirect('/articles/get/%s' % article_id)


def add_comment(req, article_id):

    a = Article.objects.get(id=article_id)
    if req.POST:
        f = CommentForm(req.POST)
        if f.is_valid():
            c = f.save(commit=False)
            c.pub_date = timezone.now()
            c.article = a
            c.save()
            return HttpResponseRedirect('/articles/get/%s' % article_id)
    else:
        f = CommentForm()

    args = {}
    args.update(csrf(req))
    args['article'] = a
    args['form'] = f

    return render_to_response('add_comment.html', args)


def search_titles(req):
    if req.method == "POST":
        search_text = req.POST['search_text']
    else:
        search_text = ''

    articles = Article.objects.filter(title__contains=search_text)

    return render_to_response("ajax_search.html", {"articles": articles})

