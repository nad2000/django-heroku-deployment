# -*- coding: utf-8 -*-

from django import forms
from models import Article, Comment


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        # which fields will be shown:
        fields = ('title', 'body', 'pub_date', 'thumbnail')


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        # which fields will be shown:
        fields = ('name', 'body')
